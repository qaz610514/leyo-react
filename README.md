# 環境需求

* 安裝 Node.js

&nbsp;

# 使用方式

`npm install` 安裝相關套件 (首次使用)

`npm start` 本地端測試，在瀏覽器上使用 [localhost:3000](http://localhost:3000)

`npm run build` 輸出專案，即可在build資料夾底下獲得

&nbsp;

# 部屬網站注意事項

1. 修改伺服器位置 

> 進入專案資料夾底下 src/Leyo/WebServer.js
>
>修改 `const WebServerURL = "{後端位置}";`
>
>Example : `const WebServerURL = "http://192.168.1.123:1234";`

2. 修改網站首頁位置

> 進入專案資料夾底下 package.json
>
>修改 `"homepage": "{首頁位置}",`
>
>Example : `"homepage": "http://192.168.1.123:1234/",`