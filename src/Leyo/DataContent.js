import {CCard, CCardBody, CCardGroup, CCardHeader, CCol, CRow, CDataTable} from "@coreui/react";
import {CSVButton} from "./CSVBtton";
import {CChartRadar} from "@coreui/react-chartjs";
import {isMobile} from "react-device-detect";
import {StageTotal} from "./StageTagMethod";
import React from "react";

const DataContent = (props) => {

  const titles = ['參數名稱', '參數內容'];

  const chartDB = props.ChartValues.map(item => item < 0.02 ? 0 : item);

  const graphValues = [
    {"參數名稱": "理解力/執行力", "參數內容": chartDB[1]},
    {"參數名稱": "抽象具象思考", "參數內容": chartDB[0]},
    {"參數名稱": "記憶", "參數內容": chartDB[5]},
    {"參數名稱": "計算", "參數內容": chartDB[4]},
    {"參數名稱": "專注力", "參數內容": chartDB[3]},
    {"參數名稱": "定向", "參數內容": chartDB[2]},
  ];

  return (
    <CCardGroup>
      <CCard>
        <CCardHeader>
          <CRow>
            <CCol>資料內容</CCol>
            <CCol><CSVButton LevelData={props.Info}/></CCol>
          </CRow>
        </CCardHeader>
        <CCardBody>
          <CChartRadar
            datasets={[
              {
                label: '參數',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                pointBackgroundColor: 'rgba(255,99,132,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(255,99,132,1)',
                tooltipLabelColor: 'rgba(255,99,132,1)',
                data: props.ChartValues,
              }
            ]}
            options={{
              aspectRatio: isMobile ? 1 : 5,
              tooltips: {
                enabled: true
              },
              legend: {
                labels: {
                  fontSize: 20
                }
              },
              scale: {
                pointLabels: {
                  fontSize: 20
                },
                ticks: {
                  min: 0,
                  max: 1
                }
              }
            }}
            labels={[
              '抽象具象思考', '理解力/執行力', '定向', '專注力', '計算', '記憶'
            ]}
          />
          <br></br>
          <CCard style={{fontSize: '1.2rem'}}>
            <CCardHeader>雷達圖數值</CCardHeader>
            <CCardBody>
              <CDataTable items={graphValues} fields={titles} bordered/>
            </CCardBody>
          </CCard>
          {StageTotal(props.Info)}
          {props.Records}
        </CCardBody>
      </CCard>
    </CCardGroup>
  );
}

export {DataContent};
