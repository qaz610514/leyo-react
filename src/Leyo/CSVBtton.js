import CsvDownloader from 'react-csv-downloader';
import React from "react";
import {CButton} from "@coreui/react";

const CSVButton = (props) => {

  const columns = [{
    id: 'ID',
    displayName: '使用者ID'
  }, {
    id: 'Name',
    displayName: '使用者名稱'
  }, {
    id: 'Email',
    displayName: '使用者電子郵件'
  }, {
    id: 'CreatTime',
    displayName: '資料紀錄時間'
  }, {
    id: 'LevelName',
    displayName: '關卡名稱'
  }, {
    id: 'ValueName',
    displayName: '參數名稱'
  }, {
    id: 'ValueContent',
    displayName: '參數內容'
  }];

  const datas = [];

  const {ID, UserName, UserEmail, CreatedAt, T, P, C, GraphValues} = props.LevelData;

  const realGraphValues = GraphValues.map(item => item < 0.02 ? 0 : item);

  datas.push({
    LevelName: '雷達圖數值',
    ValueName: "理解力/執行力",
    ValueContent: realGraphValues[0]
  }, {
    ValueName: "抽象具象思考",
    ValueContent: realGraphValues[1]
  }, {
    ValueName: "記憶",
    ValueContent: realGraphValues[2]
  }, {
    ValueName: "計算",
    ValueContent: realGraphValues[3]
  }, {
    ValueName: "專注力",
    ValueContent: realGraphValues[4]
  }, {
    ValueName: "定向",
    ValueContent: realGraphValues[5]
  })

  datas.push({
    ID: ID,
    Name: UserName,
    Email: UserEmail,
    CreatTime: CreatedAt
  }, {
    LevelName: '總結',
    ValueName: "時間",
    ValueContent: T
  }, {
    ValueName: "分數",
    ValueContent: P
  }, {
    ValueName: "金額",
    ValueContent: C
  });

  const {LevelRecords} = props.LevelData;

  for (let i = 1; i <= LevelRecords.length; i++) {
    const {T, P, C, Actions, Topic, Answer, TopicIndex, TIndex, AnswerIndex} = LevelRecords[i - 1];

    const resetIIndex = TIndex.filter(time => time > -9999);

    datas.push({
      LevelName: 'Stage' + i,
      ValueName: "時間",
      ValueContent: i === 1 ? Array.isArray(TIndex) ? '#記憶時間#' + TIndex[0] + '#作答時間#' + T : 0 : T
    }, {
      ValueName: "分數",
      ValueContent: P
    }, {
      ValueName: "金額",
      ValueContent: C
    }, {
      ValueName: "題目",
      ValueContent: Topic
    }, {
      ValueName: "作答",
      ValueContent: i === 7 ? Actions : Answer
    }, {
      ValueName: "題目分項",
      ValueContent: TopicIndex
    }, {
      ValueName: "分項時間",
      ValueContent: i === 1 ? "" : resetIIndex
    }, {
      ValueName: "分項作答",
      ValueContent: AnswerIndex
    });
  }

  const fileName = "UserDataCSV_" + UserName + '_' + ID;

  return (
    <CsvDownloader
      filename={fileName}
      extension=".csv"
      separator=","
      wrapColumnChar=""
      columns={columns}
      datas={datas}
    >
      <CButton className="float-right" color="dark">下載CSV</CButton>
    </CsvDownloader>
  );
}

export {CSVButton};
