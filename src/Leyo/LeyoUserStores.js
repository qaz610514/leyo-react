let selectIndex = 0;
let records = [];

const selectRecordIndex = () => selectIndex;
const Records = () => records;

const setSelectRecordIndex = (index) => {
  selectIndex = index;
};

const setRecords = (data) => {
  records = data;
};

export {selectRecordIndex, Records, setSelectRecordIndex, setRecords};
