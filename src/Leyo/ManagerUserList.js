import React, {Component} from 'react';
import {setAllData, setSelectUserID} from "./LeyoManagerStores";
import {CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CPagination, CRow} from "@coreui/react";
import {Link} from "react-router-dom";
import {isMobile} from "react-device-detect";
import postData from "./PostMethod";
import {GameRecordAPI} from "./WebServer";

class ManagerUserList extends Component {
  constructor(props) {
    super(props);

    const token = window.sessionStorage.getItem('userToken');

    if (!token) {
      window.location.href = '/';
    }

    postData(GameRecordAPI, {token}).then(json => {

      const {ErrorCode, Data} = json;

      if (ErrorCode !== '200') return;


      if (Data == null) {
        this.setState({preparing: false});
        return;
      }

      if (Data.length === 0) {
        this.setState({preparing: false});
        return;
      }

      //依照使用者分類
      const data = [];

      for (let i in Data) {

        const info = Data[i];

        const {UserID} = info;

        const oldData = data[UserID];

        if (oldData) {
          const {DataArray} = oldData;
          data[UserID] = {
            'UserID': UserID,
            'DataArray': [
              ...DataArray,
              info
            ]
          }
        } else {
          data[UserID] = {
            'UserID': UserID,
            'DataArray': [info]
          }
        }
      }

      //使用者ID
      const userList = Object.keys(data);

      //頁數
      const pageCount = Math.ceil(userList.length / 5);

      //儲存資料
      let dataArray = [];
      Object.keys(data).forEach(item=>dataArray.push(data[item]));

      setAllData(dataArray);

      this.setState({preparing: false, pageCount: pageCount, userList: userList, allData: data});
    })

    this.state = {
      preparing: true,
      pageCount: 0,
      currentPage: 1,
      userList: [],
      allData: null
    };
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    const preparingStatus = this.state.preparing;
    const pageCount = this.state.pageCount;
    const currentPage = this.state.currentPage;
    const userList = this.state.userList;

    if (preparingStatus) {
      return (
        <div className="animated fadeIn">
          <CCard>
            <CCardHeader>
              <div style={{fontSize: '1.5rem'}}>使用者列表</div>
            </CCardHeader>
            <CCardBody>
              載入中...
            </CCardBody>
          </CCard>
        </div>
      )
    }

    if (!preparingStatus && userList.length === 0) {
      return (
        <div className="animated fadeIn">
          <CCard>
            <CCardHeader>
              <div style={{fontSize: '1.5rem'}}>使用者列表</div>
            </CCardHeader>
            <CCardBody>
              暫無使用者資料
            </CCardBody>
          </CCard>
        </div>
      )
    }

    const handleClick = (id) => {
      setSelectUserID(id);
    }

    const titleStyle = {
      fontSize: isMobile ? '1rem' : '2rem',
      height: '60px',
      lineHeight: '60px',
      textAlign: 'center'
    };

    const userInfo = (index) => {
      if (index >= userList.length) {
        return (
          <CCard style={{
            visibility: 'hidden'
          }}
          >
            <CButton color="light">
              <div style={titleStyle}>
                {index}
              </div>
            </CButton>
          </CCard>
        );
      } else {

        const id = userList[index];

        const {UserName} = this.state.allData[id].DataArray[0];

        return (
          <Link from='/ManagerUserList' to='/ManagerUserList/ManagerInfoList'>
            <CCard>
              <CButton color="light" onClick={() => {
                handleClick(id);
              }}>
                <CRow style={titleStyle}>
                  <CCol>使用者 : {UserName}</CCol>
                </CRow>
              </CButton>
            </CCard>
          </Link>
        );
      }
    }

    return (
      <div className="animated fadeIn">
        <CCard>
          <CCardHeader>
            <div style={{fontSize: '1.5rem'}}>使用者列表</div>
          </CCardHeader>
          <CCardBody>
            {userInfo((currentPage - 1) * 5)}
            {userInfo((currentPage - 1) * 5 + 1)}
            {userInfo((currentPage - 1) * 5 + 2)}
            {userInfo((currentPage - 1) * 5 + 3)}
            {userInfo((currentPage - 1) * 5 + 4)}
          </CCardBody>
          <CCardFooter>
            <CPagination
              size="lg"
              align="center"
              activePage={currentPage}
              pages={pageCount}
              onActivePageChange={num => {
                this.setState({currentPage: num});
              }}
            />
          </CCardFooter>
        </CCard>
      </div>
    );
  }

}

export default ManagerUserList;
