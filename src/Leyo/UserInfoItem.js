import React, {Component} from "react";
import {selectRecordIndex, Records} from "./LeyoUserStores";
import {StageContent} from "./StageTagMethod";
import {DataContent} from "./DataContent";

class UserInfoItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    const token = window.sessionStorage.getItem('userToken');

    if (!token) {
      window.location.href = '/';
    }

  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    const info = Records()[selectRecordIndex()];

    if (!info) {
      window.location.href = '/';
    }

    const {LevelRecords, GraphValues} = info;

    const chartValues = [GraphValues[1], GraphValues[0], GraphValues[5], GraphValues[4], GraphValues[3], GraphValues[2]];

    const records = Array.from({length: 12}, (_, index) => <StageContent key={index} LevelNum={index + 1}
                                                                         Data={LevelRecords[index]}/>);
    return (
      <DataContent Info={info} ChartValues={chartValues} Records={records}></DataContent>
    );
  }
}

export default UserInfoItem;
