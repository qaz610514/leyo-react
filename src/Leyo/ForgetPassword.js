import React, {useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {ForgetPasswordAPI} from "./WebServer";

const ForgetPassword = () => {

  const [email, setEmail] = useState();

  //0:init  1:Loading  2:Success  3:Fail
  const [fetchStatus, setFetchStatus] = useState(0);

  const FetchRequest = () => {

    if (!email) return;

    setFetchStatus(1);

    const url = ForgetPasswordAPI + '/' + email;
    fetch(url).then(response => {
      return response.json()
    }).then((json) => {

      const {ErrorCode} = json;

      if (ErrorCode === '200') {
        setFetchStatus(2);
      } else {
        setFetchStatus(3);
      }
    })
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>忘記密碼</h1>
                    <p className="text-muted">請輸入您註冊時的電子郵件信箱(等同於登入時的帳號)，透過接收電子郵件進行密碼重製!</p>
                    <div style={{
                      display: fetchStatus === 0 ? '' : 'none'
                    }}>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user"/>
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="text" placeholder="Email" autoComplete="username"
                                onChange={(content) => {
                                  setEmail(content.target.value);
                                }}
                        />
                      </CInputGroup>
                      <CRow style={{
                        justifyContent: 'center'
                      }}>
                        <CButton
                          color="primary"
                          className="px-4"
                          onClick={() => {
                            FetchRequest();
                          }}
                        >
                          發送信件
                        </CButton>
                      </CRow>
                    </div>
                    <div style={{
                      display: fetchStatus === 1 ? '' : 'none',
                      color: 'red',
                      fontSize: '1.5em',
                    }}>
                      <CRow style={{
                        justifyContent: 'center',
                      }}>
                        載入中...
                      </CRow>
                    </div>
                    <div style={{
                      display: fetchStatus === 2 ? '' : 'none',
                      color: 'red',
                      fontSize: '1.5em',
                    }}>
                      <CRow style={{
                        justifyContent: 'center',
                      }}>
                        電子郵件寄送成功，您將可以在電子信箱中收取更改密碼的網址!
                      </CRow>
                    </div>
                    <div style={{
                      display: fetchStatus === 3 ? '' : 'none',
                      color: 'red',
                      fontSize: '1.5em',
                    }}>
                      <CRow style={{
                        justifyContent: 'center',
                      }}>
                        此Email尚未註冊
                      </CRow>
                    </div>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default ForgetPassword
