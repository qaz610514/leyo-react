function postData(url, data) {
  return fetch(url, {
    body: JSON.stringify(data),
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'content-type': 'application/json'
    },
    method: 'POST',
    mode: "cors",
    redirect: "follow",
    referrer: "no-referrer",
  })
    .then(response => response.json())
}

export default postData;
