//const WebServerURL = "http://13.115.172.15:9487";
//const WebServerURL = "http://192.168.50.77:9487";
const WebServerURL = "http://52.196.71.137:9487";

//登入
const LoginAPI = WebServerURL + '/Account/Internal/Login';

//確認身分 1:管理者 0:一般使用者
const AuthorityAPI = WebServerURL + '/Account/Authority';

//使用者基本資料
const UserDataAPI = WebServerURL + '/Account/GetUserData';

//遊戲紀錄
const GameRecordAPI = WebServerURL + '/Account/List';

//忘記密碼
const ForgetPasswordAPI = WebServerURL + '/Account/MailReset';

//重製密碼
const ResetPasswordAPI = WebServerURL + '/Account/ResetPassWord';

export {LoginAPI, AuthorityAPI, UserDataAPI, GameRecordAPI, ForgetPasswordAPI, ResetPasswordAPI};
