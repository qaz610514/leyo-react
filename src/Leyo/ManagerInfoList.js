import React, {Component} from 'react';
import {CRow, CCol, CCard, CCardHeader, CCardBody, CCardFooter, CPagination, CButton} from '@coreui/react'
import {Link} from 'react-router-dom'
import {AllData, SelectUserID, setSelectRecordIndex} from "./LeyoManagerStores";
import {isMobile} from "react-device-detect";

class ManagerInfoList extends Component {
  constructor(props) {
    super(props);

    const token = window.sessionStorage.getItem('userToken');

    if (!token) {
      window.location.href = '/';
    }

    const userID = SelectUserID();

    if (!userID) {
      window.location.href = '/';
    }

    const {DataArray} = AllData().filter(item=> item.UserID === userID)[0];

    const pageCount = Math.ceil(DataArray.length / 5);

    this.state = {
      pageCount: pageCount,
      currentPage: 1,
      gameRecords: DataArray
    };
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    const pageCount = this.state.pageCount;
    const currentPage = this.state.currentPage;
    const records = this.state.gameRecords;

    if (records.length === 0) {
      return (
        <div className="animated fadeIn">
          <CCard>
            <CCardHeader>
              <div style={{fontSize: '1.5rem'}}>紀錄列表</div>
            </CCardHeader>
            <CCardBody>
              暫無遊戲紀錄
            </CCardBody>
          </CCard>
        </div>
      )
    }

    const titleStyle = {
      fontSize: isMobile ? '1rem' : '2rem',
      height: '60px',
      lineHeight: '60px',
      textAlign: 'center'
    };

    const handleClick = (index) => {
      setSelectRecordIndex(index);
    }

    const getGameInfo = (index) => {

      if (index >= records.length) {
        return (
          <CCard style={{
            visibility: 'hidden'
          }}
          >
            <CButton color="light">
              <div style={titleStyle}>
                {index}
              </div>
            </CButton>
          </CCard>
        );
      } else {

        const {CreatedAt, P} = records[index];

        if (isMobile) {
          return (
            <Link from='/ManagerUserList/ManagerInfoList' to='/ManagerUserList/ManagerInfoList/ManagerInfoItem'>
              <CCard>
                <CButton color="light" onClick={() => {
                  handleClick(index);
                }}>
                  <CRow style={titleStyle}>
                    遊戲時間 : {CreatedAt}
                  </CRow>
                  <CRow style={titleStyle}>
                    總分 : {P}
                  </CRow>
                </CButton>
              </CCard>
            </Link>
          );
        }

        return (
          <Link from='/ManagerUserList/ManagerInfoList' to='/ManagerUserList/ManagerInfoList/ManagerInfoItem'>
            <CCard>
              <CButton color="light" onClick={() => {
                handleClick(index);
              }}>
                <CRow style={titleStyle}>
                  <CCol>遊戲時間 : {CreatedAt}</CCol>
                  <CCol>總分 : {P}</CCol>
                </CRow>
              </CButton>
            </CCard>
          </Link>
        );
      }
    }

    return (
      <div className="animated fadeIn">
        <CCard>
          <CCardHeader>
            <div style={{fontSize: '1.5rem'}}>紀錄列表</div>
          </CCardHeader>
          <CCardBody>
            {getGameInfo((currentPage - 1) * 5)}
            {getGameInfo((currentPage - 1) * 5 + 1)}
            {getGameInfo((currentPage - 1) * 5 + 2)}
            {getGameInfo((currentPage - 1) * 5 + 3)}
            {getGameInfo((currentPage - 1) * 5 + 4)}
          </CCardBody>
          <CCardFooter>
            <CPagination
              size="lg"
              align="center"
              activePage={currentPage}
              pages={pageCount}
              onActivePageChange={num => {
                this.setState({currentPage: num});
              }}
            />
          </CCardFooter>
        </CCard>
      </div>
    );
  }
}

export default ManagerInfoList;
