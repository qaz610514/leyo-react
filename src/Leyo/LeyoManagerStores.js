let selectUserID = 0;
let allData = [];
let selectRecordIndex;

const SelectUserID = () => selectUserID;
const AllData = () => allData;
const SelectRecordIndex = () => selectRecordIndex;

const setSelectUserID = (index) => {
  selectUserID = index;
};

const setAllData = (data) => {
  allData = data;
};

const setSelectRecordIndex = (index) => {
  selectRecordIndex = index;
}

export {SelectUserID, AllData, SelectRecordIndex,setSelectUserID, setAllData, setSelectRecordIndex};
