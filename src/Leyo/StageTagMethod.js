import {CCard, CCardBody, CCardHeader, CDataTable, CContainer, CRow, CCol, CImg} from "@coreui/react";
import React from "react";
import Gate1 from './Stage1Image/Gate1.png';
import Gate2 from './Stage1Image/Gate2.png';
import Gate3 from './Stage1Image/Gate3.png';
import Sky1 from './Stage1Image/Sky1.png';
import Sky2 from './Stage1Image/Sky2.png';
import Sky3 from './Stage1Image/Sky3.png';
import Obj1 from './Stage1Image/Obj1.png';
import Obj2 from './Stage1Image/Obj2.png';
import Obj3 from './Stage1Image/Obj3.png';

const fontStyle = {
  fontSize: '1.2rem'
};

const StageTotal = (info) => {

  const {T, P, C} = info;
  const fields = ['參數名稱', '參數內容'];
  const dataSet = [
    {'參數名稱': '總花費時間', '參數內容': T},
    {'參數名稱': '總獲得的分數', '參數內容': P},
    {'參數名稱': '總獲得的金額', '參數內容': C},
  ]
  return (
    <CCard style={fontStyle}>
      <CCardHeader>
        結算
      </CCardHeader>
      <CCardBody>
        <CDataTable
          items={dataSet}
          fields={fields}
          bordered
        />
      </CCardBody>
    </CCard>
  );
}

const StageContent = (props) => {

  const fields = ['參數名稱', '參數內容'];

  const level = props.LevelNum;

  const {T, C, P} = props.Data;

  let dataSet = [];

  if (level === 1) {

    const {TIndex} = props.Data;

    dataSet = [
      {'參數名稱': '記憶時間', '參數內容': Array.isArray(TIndex) ? TIndex.length > 0 ? TIndex[0] : 0 : 0},
      {'參數名稱': '作答時間', '參數內容': T},
      {'參數名稱': '獲得的分數', '參數內容': P},
      {'參數名稱': '獲得的金錢數目', '參數內容': C}
    ];
  } else {
    dataSet = [
      {'參數名稱': '花費時間', '參數內容': T},
      {'參數名稱': '獲得的分數', '參數內容': P},
      {'參數名稱': '獲得的金錢數目', '參數內容': C}
    ];
  }

  return (
    <CCard style={fontStyle}>
      <CCardHeader>
        Stage{props.LevelNum}
      </CCardHeader>
      <CCardBody>
        <CDataTable
          items={dataSet}
          fields={fields}
          bordered
        />
        <CustomContent LevelNum={props.LevelNum} Data={props.Data}/>
      </CCardBody>
    </CCard>
  );
}

const CustomContent = (props) => {

  switch (props.LevelNum) {
    case 1:
      return <Level1 Data={props.Data}/>;
    case 2:
      return <Level2 Data={props.Data}/>;
    case 3:
      return <Level3 Data={props.Data}/>;
    case 4:
      return <Level4 Data={props.Data}/>;
    case 5:
      return <Level5 Data={props.Data}/>;
    case 6:
      return <Level6 Data={props.Data}/>;
    case 7:
      return <Level7 Data={props.Data}/>;
    case 8:
      return <Level8 Data={props.Data}/>;
    case 9:
      return <Level9 Data={props.Data}/>;
    case 10:
      return <Level10 Data={props.Data}/>;
    case 11:
      return <Level11 Data={props.Data}/>;
    case 12:
      return <Level12 Data={props.Data}/>;
    default:
      return <div>Developing...</div>
  }
}

const Level1 = (props) => {
  const topicTitle = ['Sky', 'Gate', 'Obj'];
  const {Topic, Answer} = props.Data;
  const topics = Topic.split('#');
  const answers = Answer === '' ? Array.from({length: 0}) : Answer.split('#');
  if (topics.length < 3 && answers.length < 3) return (
    <div>Data Error</div>
  );
  const topicImage = [];
  const answerImage = [];
  for (let i = 0; i < topics.length; i++) {
    topicImage.push(topicTitle[i] + topics[i])
    let answer = 'null';
    if (answers.length > i) answer = topicTitle[i] + answers[i];
    answerImage.push(answer);
  }

  return (
    <CCard style={fontStyle}>
      <CCardHeader>
        Records
      </CCardHeader>
      <CCardBody>
        <CContainer>
          <CRow>
            <CCol style={{color: "red"}}>題目</CCol>
          </CRow>
          <br/>
          <CRow>
            <CCol>天空圖</CCol>
            <CCol>背景圖</CCol>
            <CCol>物品圖</CCol>
          </CRow>
          <hr/>
          <Level1Image key="1" Image={topicImage}/>
          <br/>
          <CRow>
            <CCol style={{color: "red"}}>作答</CCol>
          </CRow>
          <br/>
          <CRow>
            <CCol>天空圖</CCol>
            <CCol>背景圖</CCol>
            <CCol>物品圖</CCol>
          </CRow>
          <hr/>
          <Level1Image key="2" Image={answerImage}/>
        </CContainer>
      </CCardBody>
    </CCard>
  );
}

const Level1Image = (props) => {

  const dictionary = {
    "Gate0": Gate1,
    "Gate1": Gate2,
    "Gate2": Gate3,
    "Sky0": Sky1,
    "Sky1": Sky2,
    "Sky2": Sky3,
    "Obj0": Obj1,
    "Obj1": Obj2,
    "Obj2": Obj3,
  };

  const imageNames = props.Image;

  const result = [];
  for (let i = 0; i < 3; i++) {
    if (imageNames[i] in dictionary) {
      result.push(<CCol key={i}><CImg className="img-fluid" src={dictionary[imageNames[i]]}/></CCol>)
    } else {
      result.push(<CCol key={i}>無資料</CCol>)
    }
  }

  return (
    <CRow>
      {result}
    </CRow>
  );
}

const Level2 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '作答內容', '花費時間'];
  const {TIndex, TopicIndex, AnswerIndex} = props.Data;
  const topicTitles = ['年(民國)', '月', '日', '星期', '地區', '角色', '夜市'];
  let dataSet = [];

  for (let i = 0; i < TopicIndex.length; i++) {
    let topic = TopicIndex[i];
    let answer = '未作答';
    let time = '-';
    if (TIndex.length > i) time = TIndex[i];
    if (AnswerIndex.length > i) answer = AnswerIndex[i];
    if (time < -9999) time = "未操作";
    const check = topic === answer;
    dataSet.push({
      '參數名稱': topicTitles[i],
      '參數內容': check,
      '題目內容': topic,
      '作答內容': answer,
      '花費時間': time < -9999 ? "未作答" : time
    });
  }
  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level3 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '作答內容', '花費時間'];
  const {Topic, TIndex, TopicIndex, AnswerIndex} = props.Data;
  const topics = Topic.split('#');
  if (topics.length !== 5 || TopicIndex.length !== 5) return <div>Data Error</div>;
  let dataSet = [];
  const topic = topics.reduce((pre, current) => pre + '=>' + current);
  dataSet.push(
    {
      '參數名稱': '總題目',
      '參數內容': '',
      '題目內容': topic,
      '作答內容': '',
      '花費時間': ''
    });
  for (let i = 0; i < 5; i++) {
    let answer = '未作答';
    let time = 0;
    const levelTopic = TopicIndex[i].split('#').reduce((pre, current) => pre + '=>' + current);
    if (AnswerIndex.length > i) answer = AnswerIndex[i].split('#').reduce((pre, current) => pre + '=>' + current);
    if (TIndex.length > i) time = TIndex[i];
    const check = levelTopic === answer;
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '小題',
      '參數內容': check,
      '題目內容': levelTopic,
      '作答內容': answer,
      '花費時間': time < -9999 ? "未作答" : time
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level4 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '作答內容', '花費時間'];
  const {TIndex, TopicIndex, AnswerIndex} = props.Data;
  let dataSet = [];
  for (let i = 0; i < 3; i++) {
    let time = '未作答';
    let topic = '未作答';
    let answer = '未作答';
    if (TIndex.length > i) time = TIndex[i];
    if (TopicIndex.length > i) topic = TopicIndex[i];
    if (AnswerIndex.length > i) answer = AnswerIndex[i];
    const check = topic === answer;
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '題',
      '參數內容': check,
      '題目內容': topic,
      '作答內容': answer,
      '花費時間': time < -9999 ? "未作答" : time
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level5 = (props) => {
  let fields = ['參數名稱', '參數內容', '花費時間'];
  const {TIndex, TopicIndex, AnswerIndex, P} = props.Data;
  let dataSet = [];
  dataSet.push({
    '參數名稱': '題目',
    '參數內容': TopicIndex.length === 0 ? "" : TopicIndex.reduce((pre, current) => pre + '、' + current),
    '花費時間': ''
  })
  dataSet.push({
    '參數名稱': '答對數',
    '參數內容': P,
    '花費時間': ''
  })
  for (let i = 0; i < 3; i++) {
    let answer = '未作答';
    let time = '未作答';
    if (AnswerIndex !== null && AnswerIndex.length > i) answer = AnswerIndex[i];
    if (TIndex !== null && TIndex.length > i) time = TIndex[i];
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '次動作',
      '參數內容': answer,
      '花費時間': time < -9999 ? "未作答" : time
    })
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level6 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '作答內容'];
  const {TopicIndex, AnswerIndex} = props.Data;
  const topicTitles = ['姓名', '電話', '生日 年', '生日 月', '生日 日', '居住地區', '年紀'];
  let dataSet = [];
  for (let i = 0; i < TopicIndex.length - 2; i++) {
    let topic = '未作答';
    let answer = '未作答';
    if (TopicIndex.length > i) topic = TopicIndex[i].split('#')[1];
    if (AnswerIndex.length > i) answer = AnswerIndex[i].split('#')[1];
    const check = topic === answer;
    dataSet.push({
      '參數名稱': topicTitles[i],
      '參數內容': check,
      '題目內容': topic,
      '作答內容': answer === '' ? "未作答" : answer
    });
  }

  //居住地
  let location = '未作答';
  if (AnswerIndex.length > 5) location = AnswerIndex[5].split('#')[1];
  dataSet.push({
    '參數名稱': topicTitles[5],
    '參數內容': location !== '',
    '題目內容': '未設定',
    '作答內容': location === '' ? "未作答" : location
  });

  //年紀
  let age = '未作答';
  let ageTopic = '未設定';
  let check = false;
  if (AnswerIndex.length > 6) age = AnswerIndex[6].split('#')[1];
  if (TopicIndex.length > 6) {
    const list = TopicIndex[6].split('#');
    ageTopic = list[1] + ' - ' + list[2];
    const answerAge = parseInt(age, 10);
    const ageUp = parseInt(list[1], 10);
    const ageDown = parseInt(list[2], 10);
    if (isNaN(answerAge) || isNaN(ageUp) || isNaN(ageDown)) {

    } else {
      if (answerAge >= ageUp && answerAge <= ageDown) check = true;
    }
  }
  dataSet.push({
    '參數名稱': topicTitles[6],
    '參數內容': check,
    '題目內容': ageTopic,
    '作答內容': age === '' ? "未作答" : age
  });


  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level7 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '作答內容'];
  const {Topic, TopicIndex, AnswerIndex, Actions} = props.Data;
  let dataSet = [];
  dataSet.push({
    '參數名稱': '正確次數',
    '參數內容': Actions === '' ? 0 : Actions,
    '題目內容': Topic,
    '作答內容': '',
  });

  for (let i = 0; i < TopicIndex.length; i++) {
    let topic = '未作答';
    let answer = '未作答';
    let check = false;
    if (TopicIndex.length > i) topic = TopicIndex[i];
    if (AnswerIndex.length > i) {
      const content = AnswerIndex[i].split('#');
      answer = content.length < 2 ? "未作答" : content[0];
      check = content.length < 2 ? "false" : content[1];
    }
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '題',
      '參數內容': check === 'empty' ? '-' : check,
      '題目內容': topic,
      '作答內容': answer
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level8 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '作答內容', '花費時間'];
  const {TIndex, TopicIndex, AnswerIndex} = props.Data;
  let dataSet = [];

  if (TopicIndex.length !== 4) return <div>Data Error</div>

  for (let i = 0; i < TopicIndex.length; i++) {
    let time = '未作答';
    let topic = '未作答';
    let answer = '未作答';
    if (TIndex.length > i) time = TIndex[i];
    if (TopicIndex.length > i) topic = TopicIndex[i];
    if (AnswerIndex.length > i) answer = AnswerIndex[i];
    const check = topic === answer;
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '題',
      '參數內容': check,
      '題目內容': topic,
      '作答內容': answer,
      '花費時間': time < -9999 ? "未作答" : time
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level9 = (props) => {
  let fields = ['參數名稱', '參數內容', '花費時間'];
  const {TIndex, TopicIndex} = props.Data;
  let dataSet = [];

  for (let i = 0; i < 5; i++) {
    let time = '未作答';
    let topic = '未作答';
    if (TIndex.length > i) time = TIndex[i];
    if (TopicIndex.length > i) topic = TopicIndex[i];
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '動作',
      '參數內容': topic,
      '花費時間': time < -9999 ? "未作答" : time
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level10 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '作答內容', '花費時間'];
  const {Topic, TIndex, TopicIndex, AnswerIndex} = props.Data;
  const topics = Topic.split('#');
  let dataSet = [];
  dataSet.push({
    '參數名稱': '題目',
    '參數內容': '',
    '題目內容': Topic.replaceAll('#', '、'),
    '作答內容': '',
    '花費時間': ''
  });
  for (let i = 0; i < topics.length; i++) {
    let time = '未作答';
    let topic = '未產生';
    let answer = '未作答';
    let check = false;
    if (TIndex.length > i) time = TIndex[i];
    if (TopicIndex.length > i) topic = TopicIndex[i];
    if (AnswerIndex.length > i) {
      const content = AnswerIndex[i].split('#');
      check = content[0];
      answer = content[1];
    }
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '題',
      '參數內容': check,
      '題目內容': topic.replaceAll('#', '、'),
      '作答內容': answer,
      '花費時間': time < -9999 ? "未作答" : time
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level11 = (props) => {
  let fields = ['參數名稱', '花費時間'];
  const {TIndex} = props.Data;
  let dataSet = [];
  for (let i = 0; i < 3; i++) {
    let time = '未作答';
    if (TIndex.length > i) time = TIndex[i];
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '積木',
      '花費時間': time < -9999 ? "未作答" : time,
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Level12 = (props) => {
  let fields = ['參數名稱', '參數內容', '題目內容', '正確解答', '作答內容', '花費時間'];
  const {TIndex, TopicIndex, AnswerIndex} = props.Data;
  let dataSet = [];
  for (let i = 0; i < TopicIndex.length; i++) {
    let time = '未作答';
    let topic = '未作答';
    let price = '未作答';
    let correctAnswer = '未作答';
    let answer = '未作答';
    let check = false;
    if (TIndex.length > i) time = TIndex[i];
    if (TopicIndex.length > i) {
      const data = TopicIndex[i].split('#');
      topic = data[0];
      price = data[1];
    }
    if (AnswerIndex.length > i) {
      const data = AnswerIndex[i].split('#');
      check = data[0];
      correctAnswer = data[1];
      answer = data[2];
    }
    dataSet.push({
      '參數名稱': '第' + (i + 1) + '題',
      '參數內容': check,
      '題目內容': price + '元' + topic,
      '正確解答': correctAnswer,
      '作答內容': answer,
      '花費時間': time < -9999 ? "未作答" : time
    });
  }

  return <Table Title={'Records'} DataSet={dataSet} Fields={fields}/>;
}

const Table = (props) => {

  const {Title, DataSet, Fields} = props;

  return (
    <CCard style={fontStyle}>
      <CCardHeader>
        {Title}
      </CCardHeader>
      <CCardBody>
        <CDataTable
          items={DataSet}
          fields={Fields}
          bordered
        />
      </CCardBody>
    </CCard>
  );
}


export {StageTotal, StageContent};
