import React, {useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CLabel,
  CRow
} from '@coreui/react'
import {useParams} from "react-router-dom";
import postData from "./PostMethod";
import {ResetPasswordAPI} from "./WebServer";

const ResetContent = (props) => {

  const code = props.Is.messageCode;

  let message = () => {
    switch (code) {
      case 1:
        return "密碼不能為空!";
      case 2:
        return "密碼不同!";
      default:
        return "";
    }
  };

  return (
    <div>
      <p className="text-muted">輸入您的新密碼</p>
      <CLabel>新密碼:</CLabel>
      <CInput key="password1" name="password1" type="text" placeholder="輸入密碼" onChange={props.HandleF}
              value={props.Is.password1}/>
      <br/>
      <CLabel>再次輸入新密碼:</CLabel>
      <CInput key="password2" name="password2" type="text" placeholder="再次輸入密碼" onChange={props.HandleF}
              value={props.Is.password2}/>
      <br/>
      <CRow style={{
        justifyContent: 'center',
        color: 'red',
        display: code === 0 ? 'none' : ''
      }}>
        {message()}
      </CRow>
      <br/>
      <CRow style={{
        justifyContent: 'center'
      }}>
        <CButton color="primary" className="px-4" onClick={props.S}>
          確定
        </CButton>
      </CRow>
    </div>
  );
}

const LoadingContent = () => {
  return (
    <div>
      <br/>
      <h1 style={{color: 'red', textAlign: 'center'}}>Loading...</h1>
    </div>
  );
}

const FailContent = () => {
  return (
    <div>
      <br/>
      <h1 style={{color: 'red', textAlign: 'center'}}>重製失敗!</h1>
    </div>
  );
}

const SuccessContent = () => {
  return (
    <div>
      <br/>
      <h1 style={{color: 'red', textAlign: 'center'}}>重製成功!</h1>
      <br/>
      <CRow style={{justifyContent: 'center'}}>
        <CButton color="primary" className="px-4" onClick={() => {
          window.location.href = "/";
        }}>
          前往後臺
        </CButton>
      </CRow>
    </div>
  );
}

const Content = (props) => {
  switch (props.StatusNum) {
    case 0:
      return <ResetContent Is={props.Inputs} HandleF={props.CallbackHandle} S={props.ClickF}/>;
    case 1:
      return <LoadingContent/>;
    case 2:
      return <FailContent/>;
    case 3:
      return <SuccessContent/>;
    default:
      return <div>發生了些錯誤...</div>;
  }
}

const ResetPassword = () => {

  const {userId} = useParams();

  const [Status, setStatus] = useState(0);

  const [inputs, setInputs] = useState({
    password1: "",
    password2: "",
    messageCode: 0
  });

  const onChangeHandler = (props) => {
    const {name, value} = props.target;
    const tempData = {...inputs, [name]: value};
    const data = {...tempData, messageCode: convertMessageCode(tempData)};
    setInputs(data);
  };

  const convertMessageCode = (props) => {
    const i1 = props.password1;
    const i2 = props.password2;

    if (i1 === i2) {
      if (i1 === "") {
        return 1;
      }
      return 0;
    }

    return 2;
  }

  const SubmitEvent = () => {

    if (inputs.messageCode !== 0) {
      return;
    }

    if (inputs.password1 === inputs.password2 && inputs.password1 === "") {
      setInputs({...inputs, messageCode: 1});
      return;
    }

    const data = {userid: userId, password: inputs.password1};

    setStatus(1);

    postData(ResetPasswordAPI, data).then(json => {
      const {ErrorCode} = json;
      if (ErrorCode === '200') {
        setStatus(3);
      } else {
        setStatus(2);
      }
    }).catch(() => {
      setStatus(4);
    });
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>重製密碼</h1>
                    <Content StatusNum={Status} Inputs={inputs} CallbackHandle={onChangeHandler} ClickF={SubmitEvent}/>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default ResetPassword
