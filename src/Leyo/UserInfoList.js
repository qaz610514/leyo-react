import React, {Component} from 'react';
import {CRow, CCol, CCard, CCardHeader, CCardBody, CCardFooter, CPagination, CButton} from '@coreui/react'
import {Link} from 'react-router-dom'
import {setSelectRecordIndex, setRecords} from './LeyoUserStores';
import {isMobile} from 'react-device-detect';
import {GameRecordAPI} from "./WebServer";
import postData from "./PostMethod";

class UserInfoList extends Component {
  constructor(props) {
    super(props);

    const token = window.sessionStorage.getItem('userToken');

    if (!token) {
      window.location.href = '/';
    }

    postData(GameRecordAPI, {token}).then(json => {

      const {ErrorCode, Data} = json;

      if (ErrorCode !== '200') return;

      if(Data == null){

        this.setState({preparing:false});

        return;
      }

      //頁數
      const pageCount = Math.ceil(Data.length / 5);

      //儲存資料
      setRecords(Data);

      this.setState({preparing:false,pageCount: pageCount, gameRecords: Data});
    });

    // setTimeout(()=>{
    //
    //   const Data = [{"T":235.65374445915222,"P":31,"C":395,"ID":null,"UserName":8787,"UserEmail":"???","CreatedAt":"???","GraphValues":[0.7,0.742857158,0.5714286,0.6666667,0.64,0.7571429],"LevelRecords":[{"T":3.0350050926208496,"P":0,"C":0,"TIndex":[],"Roles":null,"Inputs":null,"Actions":null,"Topic":"0#0#0","Answer":"1#0#0","TopicIndex":[],"AnswerIndex":[]},{"T":15.042744874954224,"P":4,"C":40,"TIndex":[],"Roles":["玲玲"],"Inputs":[4.314316511154175,6.345128059387207,9.651331186294556,-1637582665.7813666,-1637582665.7813666],"Actions":null,"Topic":"","Answer":"","TopicIndex":["110","11","22","星期一","玲玲"],"AnswerIndex":["110","11","22","星期一","玲玲"]},{"T":16.681824684143066,"P":5,"C":50,"TIndex":[2.260089874267578,3.9411983489990234,2.9998412132263184,2.6196370124816895,4.861058235168457],"Roles":null,"Inputs":null,"Actions":null,"Topic":"串燒#水蜜桃#雪花冰#章魚燒#海苔壽司","Answer":"","TopicIndex":["開始#串燒","串燒#水蜜桃","水蜜桃#雪花冰","雪花冰#章魚燒","章魚燒#海苔壽司"],"AnswerIndex":["開始#串燒","串燒#水蜜桃","水蜜桃#雪花冰","雪花冰#章魚燒","章魚燒#海苔壽司"]},{"T":2.8687124252319336,"P":3,"C":50,"TIndex":[2.568263292312622,0.5951342582702637,0.8401899337768555,-1637582708.069445],"Roles":null,"Inputs":null,"Actions":null,"Topic":"","Answer":"","TopicIndex":["火鍋","義大利麵","西瓜"],"AnswerIndex":["火鍋","義大利麵","西瓜"]},{"T":2.151486396789551,"P":3,"C":50,"TIndex":[1.3503053188323975,0.42909693717956543,0.37108397483825684],"Roles":null,"Inputs":null,"Actions":null,"Topic":"","Answer":"","TopicIndex":["香菇","南瓜","玉米"],"AnswerIndex":["香菇","南瓜","玉米"]},{"T":15.834155797958374,"P":5,"C":25,"TIndex":[],"Roles":null,"Inputs":null,"Actions":null,"Topic":"","Answer":"","TopicIndex":["姓名#ttt1","電話#12345678","年#107","月#2","日#2","居住地區#","年紀#2#4"],"AnswerIndex":["姓名#ttt1","電話#12345678","年#108","月#2","日#2","居住地區#8787","年紀#3"]},{"T":51.016146421432495,"P":0,"C":0,"TIndex":[],"Roles":null,"Inputs":null,"Actions":"6","Topic":"串燒","Answer":"","TopicIndex":["蝦子壽司","串燒","串燒","鮪魚壽司","海苔壽司","串燒","章魚壽司","鮪魚壽司","串燒","串燒","章魚壽司","鮪魚壽司","蝦子壽司","串燒","鮭魚壽司","串燒","玉米","串燒","玉米","水蜜桃","串燒","章魚壽司","玉米","水蜜桃","水蜜桃","串燒","海苔壽司","蝦子壽司","鮭魚卵壽司","水蜜桃"],"AnswerIndex":["舉手#false","舉手#true","舉手#true","舉手#false","舉手#false","舉手#true","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty","未舉手#empty"]},{"T":2.9952895641326904,"P":1,"C":50,"TIndex":[1.4829471111297607,0.0780179500579834,0.22005009651184082,0.38008546829223633],"Roles":null,"Inputs":null,"Actions":null,"Topic":"","Answer":"","TopicIndex":["蚵仔","青菜","雞蛋","麵粉糊"],"AnswerIndex":["蚵仔","青菜","雞蛋","麵粉糊"]},{"T":29.496755599975586,"P":5,"C":50,"TIndex":[0.9989457130432129,5.367505788803101,5.032268524169922,3.03178334236145,6.06286096572876],"Roles":null,"Inputs":null,"Actions":null,"Topic":"","Answer":"","TopicIndex":["點選肉片","肉翻面1","肉翻面2","灑胡椒","裝盤"],"AnswerIndex":["點選肉片","肉翻面1","肉翻面2","灑胡椒","裝盤"]},{"T":8.618110418319702,"P":3,"C":50,"TIndex":[6.251832008361816,1.237933874130249,1.1273443698883057],"Roles":null,"Inputs":null,"Actions":null,"Topic":"壽司#青菜#香菇","Answer":"","TopicIndex":["紅豆餅#水蜜桃#香菇#雞蛋#壽司","雪花冰#西瓜#蝦子#章魚燒#青菜","章魚燒#雪花冰#香菇#玉米#紅豆餅"],"AnswerIndex":["true#壽司","true#青菜","true#香菇"]},{"T":21.929528951644897,"P":0,"C":0,"TIndex":[10.134028196334839,5.050330400466919,6.74517035484314,-1637582860.4903014],"Roles":null,"Inputs":null,"Actions":null,"Topic":"","Answer":"","TopicIndex":[],"AnswerIndex":[]},{"T":36.63499975204468,"P":2,"C":30,"TIndex":[1.977672815322876,5.798701286315918,21.89849066734314,6.960134983062744,-1637582897.87347],"Roles":null,"Inputs":null,"Actions":null,"Topic":"","Answer":"","TopicIndex":["糖果#7","氣球#9","糖葫蘆#17","爆米花#29"],"AnswerIndex":["true#93#93","true#84#84","false#67#77","false#48#46"]}]}];
    //
    //   //頁數
    //     const pageCount = Math.ceil(Data.length / 5);
    //
    //     //儲存資料
    //     setRecords(Data);
    //
    //     this.setState({preparing:false,pageCount: pageCount, gameRecords: Data});
    //
    //   },1000);

    this.state = {
      preparing:true,
      pageCount: 0,
      currentPage: 1,
      gameRecords: []
    };
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    const preparingStatus = this.state.preparing;
    const pageCount = this.state.pageCount;
    const currentPage = this.state.currentPage;
    const records = this.state.gameRecords;

    if(preparingStatus){
      return (
        <div className="animated fadeIn">
          <CCard>
            <CCardHeader>
              <div style={{fontSize: '1.5rem'}}>紀錄列表</div>
            </CCardHeader>
            <CCardBody>
              載入中...
            </CCardBody>
          </CCard>
        </div>
      )
    }

    if (!preparingStatus && records.length === 0) {
      return (
        <div className="animated fadeIn">
          <CCard>
            <CCardHeader>
              <div style={{fontSize: '1.5rem'}}>紀錄列表</div>
            </CCardHeader>
            <CCardBody>
              暫無遊戲紀錄
            </CCardBody>
          </CCard>
        </div>
      )
    }

    const titleStyle = {
      fontSize: isMobile ? '1rem' : '2rem',
      height: '60px',
      lineHeight: '60px',
      textAlign: 'center'
    };

    const handleClick = (index) => {
      setSelectRecordIndex(index);
    }

    const getGameInfo = (index) => {

      if (index >= records.length) {
        return (
          <CCard style={{
            visibility: 'hidden'
          }}
          >
            <CButton color="light">
              <div style={titleStyle}>
                {index}
              </div>
            </CButton>
          </CCard>
        );
      } else {

        const {CreatedAt, P} = records[index];

        if (isMobile) {
          return (
            <Link from='/UserInfoList' to='/UserInfoList/UserInfoItem'>
              <CCard>
                <CButton color="light" onClick={() => {
                  handleClick(index);
                }}>
                  <CRow style={titleStyle}>
                    遊戲時間 : {CreatedAt}
                  </CRow>
                  <CRow style={titleStyle}>
                    總分 : {P}
                  </CRow>
                </CButton>
              </CCard>
            </Link>
          );
        }

        return (
          <Link from='/UserInfoList' to='/UserInfoList/UserInfoItem'>
            <CCard>
              <CButton color="light" onClick={() => {
                handleClick(index);
              }}>
                <CRow style={titleStyle}>
                  <CCol>遊戲時間 : {CreatedAt}</CCol>
                  <CCol>總分 : {P}</CCol>
                </CRow>
              </CButton>
            </CCard>
          </Link>
        );
      }
    }

    return (
      <div className="animated fadeIn">
        <CCard>
          <CCardHeader>
            <div style={{fontSize: '1.5rem'}}>紀錄列表</div>
          </CCardHeader>
          <CCardBody>
            {getGameInfo((currentPage - 1) * 5)}
            {getGameInfo((currentPage - 1) * 5 + 1)}
            {getGameInfo((currentPage - 1) * 5 + 2)}
            {getGameInfo((currentPage - 1) * 5 + 3)}
            {getGameInfo((currentPage - 1) * 5 + 4)}
          </CCardBody>
          <CCardFooter>
            <CPagination
              size="lg"
              align="center"
              activePage={currentPage}
              pages={pageCount}
              onActivePageChange={num => {
                this.setState({currentPage: num});
              }}
            />
          </CCardFooter>
        </CCard>
      </div>
    );
  }
}

export default UserInfoList;
