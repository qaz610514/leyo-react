import React, {useState} from 'react'
import {Link, useHistory} from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import postData from "../../../Leyo/PostMethod";
import {AuthorityAPI, LoginAPI, UserDataAPI} from "../../../Leyo/WebServer";

const Login = () => {

  const {push} = useHistory();

  const [email, setEmail] = useState();
  const [passWord, setPassWord] = useState();

  const [loginErrorMessage, setLoginErrorMessage] = useState("");

  const handleLogin = () => {
    postData(LoginAPI, {password: passWord, username: email})
      .then(data => {
        const {ErrorCode, Data} = data;

        if (ErrorCode === '200') {
          //Success
          setLoginErrorMessage("");
          const {UserID, Token} = Data;
          //save token
          window.sessionStorage.setItem("userToken", Token);
          window.sessionStorage.setItem("userID", UserID);

          postData(UserDataAPI, {Token: Token})
            .then(data => {
              const {Data: {alias}} = data;
              window.sessionStorage.setItem("userName", alias);

              postData(AuthorityAPI, {Token: Token})
                .then(data => {
                  const {Identity} = data;
                  window.sessionStorage.setItem("userIdentity", Identity);
                  push('/');
                })
            })
        } else {
          //Fail
          if (ErrorCode === '405') {
            setLoginErrorMessage("尚未註冊");
          }
          if (ErrorCode === '400') {
            setLoginErrorMessage("密碼錯誤");
          }
        }
      })
      .catch(error => {
        setLoginErrorMessage(error);
        console.log(error);
      })
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user"/>
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" placeholder="Email" autoComplete="username"
                              onChange={(content) => {
                                setEmail(content.target.value);
                              }}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked"/>
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="password" placeholder="Password" autoComplete="current-password"
                              onChange={(content) => {
                                setPassWord(content.target.value);
                              }}/>
                    </CInputGroup>
                    <CRow style={{
                      display: loginErrorMessage ? 'flex' : 'none',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'red'
                    }}
                    >
                      {loginErrorMessage}
                    </CRow>
                    <CRow style={{
                      visibility: 'hidden'
                    }}
                    >
                      Empty
                    </CRow>
                    <CRow>
                      <CCol xs="6">
                        <CButton
                          color="primary"
                          className="px-4"
                          onClick={() => {
                            //push('/UserInfo');
                            handleLogin();
                          }}
                        >
                          Login
                        </CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <Link to='/forgetPassword'>
                          <CButton color="link" className="px-0">Forgot password?</CButton>
                        </Link>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              {/*<CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>*/}
              {/*  <CCardBody className="text-center">*/}
              {/*    <div>*/}
              {/*      <h2>Sign up</h2>*/}
              {/*      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut*/}
              {/*        labore et dolore magna aliqua.</p>*/}
              {/*      <Link to="/register">*/}
              {/*        <CButton color="primary" className="mt-3" active tabIndex={-1}>Register Now!</CButton>*/}
              {/*      </Link>*/}
              {/*    </div>*/}
              {/*  </CCardBody>*/}
              {/*</CCard>*/}
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
