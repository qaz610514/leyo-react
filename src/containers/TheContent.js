import React, {Suspense} from 'react'
import {
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import {CContainer, CFade} from '@coreui/react'

// routes config
import routes from '../routes'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

const TheContent = () => {

  //TODO Temp
  // window.sessionStorage.setItem("userToken", "8b80b4baa07b238658bb0bfe4beae5becc3ab4c10c0034292b4da333935222607cbeb9e1c3d723b0231ab5764bdbc613b0a2a8beffb9e5160bd7ae272c77a4a5807cff9ad6cc35430710cd8b212ac3a643ca19da887fee6f268df914f6745500ff7a655294dcd2df99d14ea09262396e7f9ef98dddc7eb52f32531e2cda8c884ec7d8d4e1f3dcd442976bb6d64d2f77561b6b41a2c");
  // window.sessionStorage.setItem("userID", "d25395ad8df54446b92f47f59af659b7");
  // window.sessionStorage.setItem("userName", "ttt5");
  // window.sessionStorage.setItem("userIdentity", "1");

  const contentNav = window.sessionStorage.getItem("userToken");

  const identity = window.sessionStorage.getItem("userIdentity");

  const determinePage = () => {
    let content = "/UserInfoList";
    if(identity === '1') content = '/ManagerUserList';

    return content;
  }

  return (
    <main className="c-main">
      <CContainer fluid>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route, idx) => {
              return route.component && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  render={props => (
                    <CFade>
                      <route.component {...props} />
                    </CFade>
                  )}/>
              )
            })}
            <Redirect from="/" to={contentNav ? determinePage() : "/login"}/>
          </Switch>
        </Suspense>
      </CContainer>
    </main>
  )
}

export default React.memo(TheContent)
